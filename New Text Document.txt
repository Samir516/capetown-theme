<input type="radio" name="slider" id="slide1" checked>
		<input type="radio" name="slider" id="slide2" >
		<input type="radio" name="slider" id="slide3" >
		<input type="radio" name="slider" id="slide4" >
		<div id="slides">
			<div id="overflow">
				<div class="inner">
						<div class="slide slider_1">
							<div class="slide-content">
								<h2>Slide 1</h2>
								<p>Content for slider1</p>
							</div>
						</div>

						<div class="slide slider_2">
							<div class="slide-content">
								<h2>Slide 2</h2>
								<p>Content for slider3</p>
							</div>
						</div>	

						<div class="slide slider_3">
							<div class="slide-content">
								<h2>Slide 3</h2>
								<p>Content for slider3</p>
							</div>
						</div>			
				</div>
			</div>
			<div id="controls">
				<label for="slider1"></label>
				<label for="slider2"></label>
				<label for="slider3"></label>								
			</div>
			<div id="bullets">
				<label for="slider1"></label>
				<label for="slider1"></label>
				<label for="slider1"></label>
			</div>
		</div>
